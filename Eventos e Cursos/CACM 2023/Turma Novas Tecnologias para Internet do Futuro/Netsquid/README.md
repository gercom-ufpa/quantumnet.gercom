# NetSquid

Nsse diretório você vai encontrar os códigos de exemplo e tutoriais para o Nestquid.
Os códigos estão funcionando no jupyter notebook, mas podem ser adaptados para outros formatos.

O Nestquid é um simulador de redes quânticas com foco no hadware e camada física. 
O Nestquid não é de código aberto, mas é disponibilizado uma versão para pesquisa.

O Nestquid funciona apenas em sistemas linux. Para usar no windows, você terá que utilizar o WSL ou alguma alternativa parecida.
Abaixo temos os links para ser feito a utilização do linux no windows.

# Instalação do Nestquid

1. Faça o [Registro no forum](https://forum.netsquid.org/ucp.php?mode=register) do netsquid
2. Siga o [Tutorial de Instalação](https://docs.netsquid.org/latest-release/INSTALL.html)
    
    a. Utilize o pip3 para fazer o download do nestquid;
    
    b. Use o seu Login e Senha para o comando:

```
pip3 install --user --extra-index-url https://<username>:<password>@pypi.netsquid.org netsquid
```

exemplo:

```
pip3 install --user --extra-index-url https://turmanovastec:minhasenha@pypi.netsquid.org netsquid
```
Não é necessário `--user` em ambiente virtual.

3. Após o download, teste o funcionamento do Nestquid:
```
import netsquid as ns
ns.test() 
```
# Aprendendo o Simulador:
1.  Leia a [Documentação do Simulador](https://docs.netsquid.org/latest-release/README.html)
2. Teste os [Exemplos disponiveis do gitlab](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/Turma%20Novas%20Tecnologias%20para%20Internet%20do%20Futuro/Netsquid)
3. Faça o [Tutorial Rápido](https://docs.netsquid.org/latest-release/quick_start.html) do Simulador
4. Siga os [Tutoriais](https://docs.netsquid.org/latest-release/tutorial.intro.html)
5. Faça o [exemplos](https://docs.netsquid.org/latest-release/learn.examples.html)


# Links Úteis 

* [Repositório do Netsquid](https://netsquid.org/)

* Instalação:
1. [Como Usar Linux no windows?](https://www.alura.com.br/artigos/wsl-executar-programas-comandos-linux-no-windows?gclid=Cj0KCQjwsIejBhDOARIsANYqkD0gwD3kURayCaM9i_JTzuJZ_FPUvpqLit9BzjI0S3eVMPJEd11YHGEaAqCUEALw_wcB)
2. [Como Instalar Anaconda no Linux](https://www.hostinger.com/tutorials/how-to-install-anaconda-on-ubuntu/)
3. [Como Usar o Jupyter Notebook](https://test-jupyter.readthedocs.io/en/latest/install.html)
4. [Como usar o Jupyter Notebook no WSL windows](https://mas-dse.github.io/startup/anaconda-ubuntu-install/)