# Novas Tecnologias para Internet do Futuro 

Bem-vindo ao repositório dos assuntos abordados na disciplina de Tópicos Especiais da Faculdade de Computação e Programa de Pós-Graduação em Ciência da Computação da Universidade Federal Pará em Redes ministrado pelo professor Abelém. Aqui se encontram os exemplos de código dos simuladores de Redes Quânticas trabalhados em sala de aula separados por pastas com os seus respectivos nomes.

## Lista de Conteúdo 

* Trabalho:
    * Instruções
* Simuladores:
    * [QuISP]("")
    * [QuNetSim](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/Turma%20Novas%20Tecnologias%20para%20Internet%20do%20Futuro/QuNetSim)
    * [NetSquid](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/Turma%20Novas%20Tecnologias%20para%20Internet%20do%20Futuro/Netsquid)
* Material:
    * Slides

# Instruções
1. Investigar um simulador de redes quânticas (Conforme diretrizes abaixo) e apresentar/explicar um exemplo de simulação sobre comunicação quântica.

2. As equipes devem produzir relatório técnico de 3 a 5 páginas contendo: Introdução; resumo do simulador e justificativa sobre escolha; descrição/explicação do cenário simulado; resultados obtidos; e conclusões.

3. As equipes devem entregar relatório técnico, compartilhando-o nesta plataforma até o dia 25/05/23.

4. A apresentação também deve ter Introdução; resumo do simulador e justificativa sobre escolha; descrição/explicação do cenário simulado; resultados obtidos; e conclusões.

5. As equipes que vão apresentar devem entregar apenas os slides em formato pdf.

6. Equipes selecionadas para apresentar:
    
6. Três equipes (descritas abaixo) deverão apresentar as simulações realizadas de acordo com as seguintes diretrizes:

    * As equipes SDN(1 a 4) devem investigar o simulador NetSquid (Network Simulator for Quantum Information using Discrete events)
        * Equipe SDN 2 -Selecionada para apresentar;

    * As equipes BC(1 a 3) devem investigar o simulador QuISP: Network Simulator based on Omnet++;
        * Equipe BC 3 -Selecionada para apresentar;

    * As equipes RQ(1 a 3) devem investigar o simulador QuNetSim: Network Simulator tool for python;
        * Equipe RQ 2 -Selecionada para apresentar;

8. As equipes selecionadas terão 15-20 minutos para apresentar as simulações.


# Simuladores

Os simuladores em questão são: QuNetsim, QuISP e NetSquid. Todas as pastas tem um arquivo README.md com instruções para ajudar no trabalho, isso inclui, um guia de instalação, recomendações de estudo, link da documentação e dos exemplos no código original. Além disso, também há nessas pastas os códigos para a tarefa, eles se encontram em formato Jupyter ou Python.
    
* QuNetSim
    - [Pasta do Repositório](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/Turma%20Novas%20Tecnologias%20para%20Internet%20do%20Futuro/QuNetSim)
    - [Documentação](https://github.com/tqsd/QuNetSim)
* QuISP
    - [Pasta do Repositório]("")
    - [Documentação](https://aqua.sfc.wide.ad.jp/quisp_website/)
* NetSquid
    - [Pasta do Repositório](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/Turma%20Novas%20Tecnologias%20para%20Internet%20do%20Futuro/Netsquid)
    - [Documentação](http://www.netsquid.org)
#### Nota:
Alguns códigos dos exemplos na documentação oficial de alguns simuladores não estão apresentando bom funcionamento. Os códigos aqui exibidos estão corrigidos e funcionando normalmente. No entanto, detalhes sobre as funções, métodos, classes, entre outros, estão todos na documentação de cada simulador. Por tanto, para um bom entendimento, recomenda-se que, tanto os códigos corrigidos, quanto a documentação, sejam ponderados.

# Slides

Os slides utilizados em sala de aula para ajudar nos estudos.