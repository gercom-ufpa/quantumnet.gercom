# Importando as dependências
from qunetsim.components import Host, Network
from qunetsim.objects import Qubit, Logger

# Funções para o QKD BB84
from bb84 import *

# Funções para plotar e gerenciar os dados
from plot import *

# Demais dependências
from random import randint
from time import sleep

# Probabilidade de o sniffer adquirir o qubit no enlace que espiona
p = 0

# Função utilizada para interceptar a comunicação:
def sniffing_QKD(sender, receiver, qubit):
  """
  Função utilizada pelo interceptador. Deve ser atribuída à "q_sniffing_fn" do host que irá interceptar.
  Nota: Não se passa nenhum argumento a essa função pois somente se atribui a "q_sniffing_fn", mas pode manipulá-los dentro da função.
  Além disso, para funções sniffing, só é possível usar os três parâmetros.
    
  Args: 
    sender (Host): Remetente na rede que se deseja xeretar a comunicação.
    receiver (Host): Receptor na rede que se deseja xeretar a comunicação.
    qubit (Qubit): Qubit que se deseja xeretar.
  """
  global p
  
  odd = randint(1, 100)
  if odd <= p:
    base = randint(0, 1)
    if base == 1:
      qubit.H()
    # O qubit não deve ser destruído após a medição.
    qubit.measure(non_destructive=True)
    
def main():

    # Inicializando a rede e estabelecendo as conexões.
    network = Network.get_instance()
    nodes = ['Node1', 'Node2', 'Node3', 'Node4', 'Node5', 'Node6', 'Node7', 'Node8', 'Node9', 'Node10', 'Node11', 'Node12']
    network.start(nodes)
    
    host_n1 = Host('Node1')
    host_n2 = Host('Node2')
    host_n3 = Host('Node3')
    host_n4 = Host('Node4')
    host_n5 = Host('Node5')
    host_n6 = Host('Node6')
    host_n7 = Host('Node7')
    host_n8 = Host('Node8')
    host_n9 = Host('Node9')
    host_n10 = Host('Node10')
    host_n11 = Host('Node11')
    host_n12 = Host('Node12')

    # Criando lista com os nós da rede
    hosts = [host_n1, host_n2, host_n3, host_n4, host_n5, host_n6, host_n7, host_n8, host_n9, host_n10, host_n11, host_n12]
  
    # Adicionando as conexões entre os nós da rede. A rede se parece com isso: (N1) <--> (N2) <--> ... <--> (N11) <--> (N12)
    for i, node in enumerate(hosts):
        if i == 0:
            node.add_connection(hosts[1].host_id)
        elif i < len(hosts)-1:
            node.add_connection(hosts[i-1].host_id)
            node.add_connection(hosts[i+1].host_id)
        else:
            node.add_connection(hosts[i-1].host_id)
      
    # Inicializando os Hosts
    for node in hosts:
        node.start()

    # Adicionando os hosts à rede  
    network.add_hosts(hosts)
    
    # Definindo se a rede deve ou não ser espionada
    interception = 'S'
    
    # Definindo o número de execuções simultâneas do protocolo
    execs = 4

    # Escolhendo aleatoriamente quem participa das comunicações
    senders, receivers, sniffers = choice(hosts, execs)

    # Se a rede deve ter interceptação:
    if interception == 'S':
        for pair, sniffer in zip(list(sniffers.keys()), list(sniffers.values())):
        # Se há ou não sniff
            if sniffer != 'None':
                # Escolhe pelo índice (por isso subtrair 1) o sniffer na lista de hosts
                hosts[sniffer-1].q_relay_sniffing = True
                # A função a ser aplicada aos qubits em trânsito.
                hosts[sniffer-1].q_relay_sniffing_fn = sniffing_QKD
                
    # Armazenando as informações das execuções
    execution = 0
    executions_info = []
    received_keys = dict()
    generated_keys = dict()

    key = generate_key(5)
    key_size = len(key)
    executions_info.append(f'[{execution}]:{senders[0].host_id}-{receivers[0].host_id}')
    senders[0].run_protocol(sender_QKD, (receivers[0], key, generated_keys, execution))
    receivers[0].run_protocol(receiver_QKD, (senders[0], key_size, received_keys, execution), blocking=True)
    execution += 1
    
    # Todos os resultados da simulação
    # Resultados da simulação
    results_simulations = dict()
    results_simulations[p] = [generated_keys, received_keys]


    network.stop(True)
    # Retornando dados para os gráficos
    return executions_info, generated_keys, received_keys
    #return infos, results_simulations
    

p = 0
simulations_infos = []
nodes_data = ''
generated_data = ''
received_data = ''

for i in range(10):
    p = 5*i
    infos, generated_keys, received_keys = main()
    simulations_infos.append(get_metric(generated_keys, received_keys, p))
    nodes_data += f'Nós da simulação com p igual a {p}: {infos}\n'
    generated_data += f'Chaves geradas da simulação com p igual a {p}: {generated_keys}\n'
    received_data += f'Chaves recebidas da simulação com p igual a {p}: {received_keys}\n'