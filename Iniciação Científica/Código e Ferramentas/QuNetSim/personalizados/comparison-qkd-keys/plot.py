import matplotlib.pyplot as plt
import numpy as np

def organize(qkd_keys):
    """
    Organiza o dicionário com as chaves qkd pelo índice de forma crescente.

    Args:
        qkd_keys (dict): Dicionário com índice da execução e a sua respectiva chave.
    """
    sorted_keys = {k: qkd_keys[k] for k in sorted(qkd_keys)}
    qkd_keys.clear()
    qkd_keys.update(sorted_keys)
    
def compare_keys(generated_key, received_key):
    """
    Compara duas chaves qkd bit a bit e retorna a quantidade de bits iguais. Ou seja, bits não atingidos pelos sniffer ou que foram interceptados mas colapsaram em seu valor real.

    Args:
        generated_key (list): Lista gerada da chave qkd composta de bits.
        received_key (list): Lista recebida da chave qkd composta de bits.

    Returns:
        _type_: _description_
    """
    
    # Vezes em que o qubit espionado colapsou no valor errado.
    incorrect_bits = 0
    
    for generated_bit, received_bit in zip(generated_key, received_key):
        if generated_bit != received_bit:
            incorrect_bits += 1
    return incorrect_bits

def get_metric(generated_keys, received_keys, probability):
    """
    Plota um gráfico da taxa de acerto do sniffer em função da probabilidade de ele adquirir ou não o qubit.

    Args:
        generated_keys (dict): Dicionário com todas as chaves geradas de uma simulação.
        received_keys (dict): Dicionário com todas as chaves recebidas de uma simulação.
        probability (int): Probabilidade que o sniffer tem de adquirir o qubit do enlace.
    Returns:
        metric (tuple) : Tupla com o primeiro valor correspondente a probabilidade do sniffer interceptar o qubit e o segundo valor os bits diferentes na chave recebida.
    """
    
    # Tamanho da chave vezes a quantidade de simulações.
    key_size = 256 * 4
    incorrect_bits = 0
    
    for key_gnrtd, key_rcvd in zip(generated_keys, received_keys):
        incorrect_bits += compare_keys(generated_keys[key_gnrtd], received_keys[key_rcvd])
    
    # Percentual de acertos do sniffer
    incorrect_bits_rate = (incorrect_bits * 100) / key_size
    
    # Dicionário para informações do gráfico
    metric = (probability, incorrect_bits_rate)
    
    return metric
    
def plot_graph(simulations_infos):
    """
    Plota um gráfico de acordo com as informações de uma lista que contém tuplas com as informações das simulações em função da probabilidade p diferente.
    
    for tanananna in dic com TODAS as chaves
        get_metric em cada dicionario com as chaves
    
    plotar o grafico p(taxa de erro)
    """
    
    # Eixos do gráfico
    x_axis = [] # Probabilidade do sniffer interceptar um qubit.
    y_axis = [] # Taxa de bits recebidos diferentes dos bits gerados.
    
    
    for metric in simulations_infos:
        # metric são as tuplas com as informações necessárias para o gráfico.
        # metric[0] é a probabilidade do sniffer interceptar um qubit,
        x_axis.append(metric[0])
        # metric[1] é a taxa de bits errados na chave recebida.
        y_axis.append(metric[1])
    
    # Criando o gráfico de linhas
    plt.figure(figsize=(10, 7.5))  # Tamanho da figura
    plt.plot(x_axis, y_axis, color='blue', marker='o', linestyle='-', linewidth=2, markersize=8, label='Linha de Dados')  # Gráfico de linhas
        
    # Personalizando o gráfico
    plt.title('Erro detectado vs Probabilidade do Sniffer Interceptar um Qubit')  # Título do gráfico
    plt.xlabel('Probabilidade do sniffer interceptar um qubit (%)')  # Rótulo do eixo x
    plt.ylabel('Taxa de bits recebidos diferentes dos bits gerados (%)')  # Rótulo do eixo y
        
    plt.grid(True)  # Exibir grade
    plt.legend()  # Exibir legenda
    plt.show
    
