p = 10
def organize(qkd_keys):
    """
    Organiza o dicionário com as chaves qkd pelo índice de forma crescente.

    Args:
        qkd_keys (dict): Dicionário com índice da execução e a sua respectiva chave.
    """
    sorted_keys = {k: qkd_keys[k] for k in sorted(qkd_keys)}
    qkd_keys.clear()
    qkd_keys.update(sorted_keys)
    
def compare_keys(generated_key, received_key):
    sniff_sucess = 0
    
    for generated_bit, received_bit in zip(generated_key, received_key):
        if generated_bit != received_bit:
            sniff_sucess += 1
    return sniff_sucess

def metrics():
import matplotlib.pyplot as plt
import numpy as np

# Dados de exemplo
x = np.arange(1, 51)  # Números inteiros de 1 a 10
y = np.array([15, 30, 42, 55, 68, 72, 80, 88, 92, 98])  # Valores em percentual

# Criar o gráfico
plt.plot(x, y, marker='o')

# Configurar eixos
plt.xlabel('Eixo X (Números Inteiros)')
plt.ylabel('Eixo Y (Percentual)')
plt.title('Gráfico com Eixo Y em Percentual e Eixo X em Números Inteiros')

# Configurar eixo Y para exibir valores em percentual
plt.gca().yaxis.set_major_formatter(plt.FuncFormatter(lambda y, _: f'{y:.0f}%'))

# Exibir o gráfico
plt.grid(True)
plt.show()
