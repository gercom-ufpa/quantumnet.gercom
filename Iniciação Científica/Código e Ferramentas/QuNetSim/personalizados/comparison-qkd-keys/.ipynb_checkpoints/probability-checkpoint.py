# Variável global para a probabilidade do sniffer adquirir o qubit do canal.
p = 0

import matplotlib.pyplot as plt

# Dados de exemplo
x = [1, 2, 3, 4, 5]
y = [2, 4, 1, 3, 5]

# Criando o gráfico
plt.figure(figsize=(8, 6))  # Tamanho da figura
plt.scatter(x, y, color='blue', marker='o', label='Pontos de Dados')  # Gráfico de dispersão

# Personalizando o gráfico
plt.title('Gráfico de Dispersão')  # Título do gráfico
plt.xlabel('Eixo X')  # Rótulo do eixo x
plt.ylabel('Eixo Y')  # Rótulo do eixo y
plt.grid(True)  # Exibir grade
plt.legend()  # Exibir legenda

# Mostrando o gráfico
plt.show()