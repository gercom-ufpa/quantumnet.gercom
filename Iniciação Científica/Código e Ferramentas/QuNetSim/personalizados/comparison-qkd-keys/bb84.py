# Importando as dependências
from qunetsim.components import Host, Network
from qunetsim.objects import Qubit, Logger

from random import randint

def generate_key(key_size):
    key = []
    for bit in range(key_size):
        key.append(randint(0, 1))
    return key
    
def sender_QKD(sender, receiver, key, generated_keys, execution):
    """
    Protocolo QKD BB84 para o remetente.

    Args:
        sender (Host): Objeto host que deseja enviar a chave.
        receiver (Host): Objeto host que deseja receber a chave e mensagem.
        key (list): Lista de 0s e 1s que representa a chave quântica.
        msg (str): Mensagem que será enviada.
    """
        
    # Adicionando ao dicionário com a execução e a chave qkd gerada.
    generated_keys[f'{execution}'] = key
        
    # Guarda e especifica qual qubit trabalharemos.
    seq_num = 0
    for bit in key:
        ack = False
        while ack == False:
            qubit = Qubit(sender)
            # Critérios para definir a medição dos qubits:
            base = randint(0, 1)
            #print("Remetente - Base escolhida:", base)
            if bit == 1: # Depende da key.
                qubit.X()
            if base == 1: # Depende da base.
                qubit.H()
            
            # Envio do qubit transformado de acordo com as alterações aleatórias.
            #print(f"Remetente - Eniviando o Qubit {seq_num}")
            sender.send_qubit(receiver.host_id, qubit)
            
            # Agora recebemos de Bob a base utilizada em sua medição (por meio de uma mensagem clássica).
            # Nota: "receiver" e "sender" do QKD, não dessa mensagem clássica.
            message = sender.get_classical(receiver.host_id, seq_num, wait=3)    
            #print(f"Remetente - Recebeu a mensagem: {message.content}")

            # Este padrão de sintaxe define se podemos ou não partir para o próximo qubit.
            # Nº qubit : base utilizada.
            if message != None:
              if message.content == (f'{seq_num}:{base}'):
                  # Mesma base, confirma para ir para o próximo qubit.
                  ack = True
                  # Envia uma mensagem clássica com "Nº qubit : 0", 0 confirma para o receptor que a base estava correta.
                  sender.send_classical(receiver.host_id, (f"{seq_num}:0"))

              else:
                  ack = False
                  # 1 significa que a base está errada.
                  sender.send_classical(receiver.host_id, (f"{seq_num}:1"))
  
            # Próximo qubit.    
            seq_num += 1
    
def receiver_QKD(receiver, sender, key_size, received_keys, execution):
    """
    Protocolo QKD BB84 para o receptor.

    Args:
        receiver (Host): Objeto host que deseja receber a chave e mensagem.
        sender (Host): Objeto host que deseja enviar a chave.
        key_size (int): Tamanho da chave. Utilizado para controle do laço.

    """
    
    # A key "gerada" pelo reptor.
    key_receiver = []
    # Controle do laço.
    received_counter = 0
    # Nº qubit.
    seq_num = 0

    while received_counter < key_size:
        print(key_receiver)
        # Receber o qubit enviado pelo remetente.
        qubit = receiver.get_qubit(sender.host_id, wait=2)
        while qubit == None:
            #print("Receptor - O Qubit recebido vale None.")
            qubit = receiver.get_qubit(sender.host_id, wait=3)
        #print("Receptor - Qubit recebido!")
        # Mesma lógica simples para escolha de base.
        base = randint(0, 1)
        if base == 1:
            qubit.H()
        measure = qubit.measure()
        #print(f"Receptor - Base escolhida: {base}.")
        #print(f"Receptor - Enviando mensagem: {seq_num}:{base}")

        # Envio da base utilizada para o Sender.
        # Nota: "receiver" e "sender" do QKD, não dessa mensagem clássica.
        receiver.send_classical(sender.host_id, (f'{seq_num}:{base}'))

        # Recebimento da mensagem clássica de confirmação, ou não, do uso da base correta.
        message = receiver.get_classical(sender.host_id, seq_num, wait=3)
        # get_classical retorna sender: host_id e content: conteúdo da mensagem.

        # Checando a mensagem:
        if message != None:
            if message.content == (f'{seq_num}:0'):
                # Adiciona 1 ao contador de recebimento de confirmação.
                received_counter += 1
                #print(f"Macth!\n{receiver.host_id} recebeu o {received_counter}º bit da chave secreta!\n")
                key_receiver.append(measure)
            #elif message.content == (f'{execution}:{seq_num}:1'):
                #print("Não houve match. Próximo qubit.\n")
        #else: 
            #print("Algo de errado aconteceu. Mensagem sobre a base não recebida...\n")

        # Próximo qubit
        seq_num += 1

    if len(key_receiver) == key_size:
        received_keys[f'{execution}'] = key_receiver
        print('ok')

# Função para escolher os pares participante do QKD e seus respectivos sniffers      
def choice(hosts, executions=10):
  """
  Escolhe aleatoriamente quais serão os receptores e os remetentes QKD, além dos seus respectivos sniffers, caso seja possível.
  O Snffer só existirá se houver intermediários na comunicação entre o remetente o receptor.

  Args:
    hosts (lista): Lista com os objetos hosts da rede utilizada
    executions (int): Número de execuções simultâneas do protocolo

  Returns:
    senders (list): Lista com os remetentes das chaves pelo QKD
    receivers (list): Lista com os receptores das chaves pelo QKD
    sniffers (dict): As chaves são as comunicações e os valores são o sniffer para aquela comunicação
  """
              
  # Lista com os remetentes, receptores, e seus sniffers
  senders = []
  receivers = []
  sniffers = {}
  
  # Escolhendo os remetentes e receptores do protocolo
  for choice in range(executions):
    sender = hosts[randint(0, len(hosts)-1)]
    receiver = hosts[randint(0, len(hosts)-1)]
    
    # Guardar informações dos hosts escolhidos
    sender_num = int(sender.host_id[4:])
    receiver_num = int(receiver.host_id[4:])
    
    # Receiver e Sender não devem ser o mesmo Host (não há como realizar o protocolo) ou Hosts adjacentes (não é possível um sniffer)
    while (sender_num == receiver_num) or (sender_num == receiver_num + 1) or (sender_num == receiver_num - 1):
      receiver = hosts[randint(0, len(hosts)-1)]
      receiver_num = int(receiver.host_id[4:])

    # Adicionando os nós escolhidos nas suas respectvas listas
    if len(senders) != 0:
      ok = True
      while ok:
          for send, rcv in zip(senders, receivers):
            if send == sender and rcv == receiver:
              sender = hosts[randint(0, len(hosts)-1)]
              receiver = hosts[randint(0, len(hosts)-1)]
            elif send == receiver and rcv == sender:
              sender = hosts[randint(0, len(hosts)-1)]
              receiver = hosts[randint(0, len(hosts)-1)]
            else:
              ok = False
            
    senders.append(sender)
    receivers.append(receiver)
  
  # Escolhendo os sniffers.
  for send, recv in zip(senders, receivers):
    # Obtém o número do host ID. Por exemplo, o 1 do Node1.
    sender_n = int(send.host_id[4:])
    receiver_n = int(recv.host_id[4:])
    
    # Se a comunicação seja da esquerda para direita. Por exemplo, sender é o Node1 e receiver é o Node4
    if sender_n < receiver_n:  
      sniffers[f'{sender_n}-{receiver_n}'] = randint(sender_n + 1, receiver_n - 1)

    # Se a comunicação é da direita para a esquerda:
    else:
      if sender_n - 1 != receiver_n:
        sniffers[f'{sender_n}-{receiver_n}'] = randint(receiver_n + 1, sender_n - 1)

  return senders, receivers, sniffers
            
def running(senders, receivers):
  """
  Realiza a execução dos protocolos.

  Args:
    sender (Host): Lista com os Hosts que serão os remetentes do protocolo QKD.
    receiver (Host): Lista com os Hosts que serão os receptores do protocolo QKD.
  """
  execution = 0
  executions_info = []
  received_key = dict()
  generated_key = dict()
  
  # Rodando os protocolos com os remetentes e receptores escolhidos
  for send, recv in zip(senders, receivers):
    key = generate_key(5)
    key_size = len(key)
    executions_info.append(f'[{execution}]:{send.host_id}-{recv.host_id}')
    send.run_protocol(sender_QKD, (recv, key, generated_key, execution))
    recv.run_protocol(receiver_QKD, (send, key_size, received_key, execution), blocking=True)
    execution += 1
    
  return executions_info, generated_key, received_key
