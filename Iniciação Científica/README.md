# Iniciação Científica

Nesse diretório estão os códigos e materiais produzidos durante o Programa de Iniciação Científica de Arthur Henrique Azevedo Pimentel pela Universidade Federal do Pará, que teve início em 08/2022.

# Qiskit

Qiskit é um framework em Python para a construção de circuitos de computadores quânticos. Esses circuitos tem diversas aplicações, dependendo, claro, do intuito de quem o constrói. A utilização do Qiskit contribui com o entendimento do alicerce da computação quântica e como funcionam seus algoritmos. 

# QuNetSim

O QuNetSim (Quantum Network Simulator)  é um framework de simulação open source baseado em Python para simulações de redes quânticas. O uso pretendido é que se possa desenvolver e testar aplicações e protocolos projetados para redes quânticas na rede e camada de aplicação que podem transmitir e armazenar informações quânticas. 

# PDFs

Contém os documentos criados para apresentações e trabalhos para a disciplina de Tópicos Especiais em Redes.